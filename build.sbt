import Common._

lazy val common = (project in file("."))
  .settings(name := "pivot")
  .settings(libraryDependencies ++= dependencies)
  .defaultSettings
