package a.b.c.model

case class Schema(keys: List[String])

case class Row[T](categories: List[String], value: T)

case class Dataframe[T](schema: Schema, rows: Seq[Row[T]])
