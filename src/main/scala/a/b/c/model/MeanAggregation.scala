package a.b.c.model

class MeanAggregation extends Aggregation[Double, Double] {

  override protected type V = (Double, Int)

  override def map(t: Double): (Double, Int)                               = Option(t).map(elem => (elem, 1)).getOrElse((0, 1))
  override def reduce(v1: (Double, Int), v2: (Double, Int)): (Double, Int) = (v1._1 + v2._1, v1._2 + v2._2)
  override def get(v: (Double, Int)): Double                               = v._1 / v._2

}
