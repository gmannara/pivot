package a.b.c.model

trait Aggregation[T, K] {
  protected type V
  def map(t: T): V
  def reduce(v1: V, v2: V): V
  def get( v : V) : K
}