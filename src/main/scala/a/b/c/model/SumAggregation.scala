package a.b.c.model

class SumAggregation extends Aggregation[Double, Double] {

  override protected type V = Double

  override def map(t: Double): Double = Option(t).getOrElse(0d)

  override def reduce(v1: Double, v2: Double): Double =
    v1 + v2

  override def get(v: Double): Double = v

}
