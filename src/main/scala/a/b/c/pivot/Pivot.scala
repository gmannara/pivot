package a.b.c.pivot

import a.b.c.common.Direction
import a.b.c.model.{Aggregation, Dataframe, Row}

import scala.annotation.tailrec

class Pivot[T, K](df: Dataframe[T], direction: Direction.Value)(implicit aggregation: Aggregation[T, K]) {

  val valueMap: Map[List[String], K] =
    df.rows
      .flatMap(createKeys)
      .map(_.toKeyValue)
      .groupBy(_._1)
      .mapValues { elem =>
        elem.map(_._2).map(aggregation.map).reduce(aggregation.reduce)
      }
      .mapValues(aggregation.get)

  def prettyPrint =
    valueMap
      .map(elem => PivotKey[K](elem._1, elem._2))
      .toList
      .sortBy(_.key.mkString)(Ordering.String)
      .foreach(elem => println(s"Key ${elem.key.mkString(",")} : ${elem.value}"))

  def get(keys: List[String]): Option[K] = valueMap.get(keys)

  private def createKeys(row: Row[T]): List[PivotKey[T]] = {
    val acc: List[PivotKey[T]] = List[PivotKey[T]](PivotKey[T](List(), row.value))
    direction match {
      case Direction.LEFT  => computeKeys(row.value, row.categories, acc)
      case Direction.RIGHT => computeKeys(row.value, row.categories.reverse, acc)
    }
  }

  @tailrec
  private def computeKeys(row: T, categories: List[String], acc: List[PivotKey[T]]): List[PivotKey[T]] =
    if (categories.isEmpty) acc
    else
      computeKeys(row, categories.take(categories.size - 1), acc ++ List(PivotKey(categories, row)))

}

object Pivot {

  def apply[T, K](df: Dataframe[T], direction: Direction.Value)(implicit aggregation: Aggregation[T, K]) = new Pivot[T, K](df, direction)

}
