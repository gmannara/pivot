package a.b.c

package object pivot {

  case class PivotKey[T](key: List[String], value: T)

  implicit class PivotKeyOps[T](pk: PivotKey[T]) {
    def toKeyValue = pk.key -> pk.value
  }

}
