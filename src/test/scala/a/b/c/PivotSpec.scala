package a.b.c

import a.b.c.common.Direction
import a.b.c.model.{Aggregation, Dataframe, MeanAggregation, Row, Schema, SumAggregation}
import a.b.c.pivot.Pivot
import org.scalatest.{Matchers, WordSpec}

import scala.io.Source

class PivotSpec extends WordSpec with Matchers {

  val pivotDf: Iterator[String] = Source.fromInputStream(getClass.getResourceAsStream("/input.csv")).getLines()

  val schema: Array[String] = pivotDf.next().split(",")

  val rows = pivotDf.map { line =>
    val arr = line.split(",")
    Row[Double](arr.take(arr.length - 1).toList, arr.last.toDouble)
  }

  val df = Dataframe(Schema(schema.toList), rows.toList)

  implicit val sum: Aggregation[Double, Double] = new SumAggregation

  val pivot: Pivot[Double,Double] = Pivot[Double,Double](df, Direction.LEFT)(sum)
  pivot.prettyPrint
  "Pivot object" should {

    "return all values if empty list is given" in {
      pivot.get(List()) shouldBe Some(8516d)
    }

    "return the correct numberof element given keys" in {
      pivot.get(List("Spain", "Blue", "Black")) shouldBe Some(852d)
    }

    "return the correct number of element given subset of keys" in {
      pivot.get(List("Spain", "Blue")) shouldBe Some(852d)
      pivot.get(List("Germany", "Green")) shouldBe Some(1610d)
    }

    "return the correct number of element given one key only" in {
      pivot.get(List("Spain")) shouldBe Some(2896d)
    }
  }
  "Pivot reverse" should {
    val pivotReverse: Pivot[Double,Double] = Pivot[Double,Double](df, Direction.RIGHT)

    "have the same total" in {
      pivotReverse.get(List()) shouldBe pivot.get(List())
    }

    "query backward" in {
      val keys = List("Spain", "Blue", "Black")
      pivotReverse.get(keys.reverse) shouldBe pivot.get(keys)
    }
  }

  "Pivot with mean" should {
    implicit val sum: Aggregation[Double, Double] = new MeanAggregation

    val pivot: Pivot[Double,Double] = Pivot[Double, Double](df, Direction.LEFT)

    pivot.prettyPrint
    "get mean value of all elements" in {

      pivot.get(List()).get.asInstanceOf[Double] shouldBe (df.rows.map(_.value).sum / df.rows.size)

    }
  }
}
