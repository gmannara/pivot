import sbt._
import sbt.Keys._

object Common {

  implicit class ProjectFrom(project: Project) {
    def defaultSettings: Project = project.settings(
      organization := "a.b.c",
      scalaVersion := "2.11.12",
      version := "0.1.0"
    )
  }

  private[this] val scalaTestVersion = "3.0.5"
  private val scalaTest = Seq(
    "org.scalatest" %% "scalatest" % scalaTestVersion % "test"
  )

  val dependencies: Seq[ModuleID] = scalaTest

}
